

from floodsystem.station import inconsistent_typical_range_stations

from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1F"""

    stations_list = build_station_list()
    print(inconsistent_typical_range_stations(stations_list))

if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()
