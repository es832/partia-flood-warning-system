import datetime

import matplotlib.pyplot as plt

from floodsystem.stationdata import build_station_list

from floodsystem.stationdata import update_water_levels

from floodsystem.datafetcher import fetch_measure_levels

from floodsystem.plot import plot_water_levels

from floodsystem.flood import stations_highest_rel_level


def run():

    #build stations list and update

    stations = build_station_list()

    update_water_levels(stations)

    risk_stations_level = stations_highest_rel_level(stations,5) #list of 5 most risky stations with highest water level
    print(risk_stations_level)
    risk_stations_name = []

    for station in risk_stations_level:
        risk_stations_name.append(station[0])
    
    
    risk_stations=[]
    print(risk_stations_name)

    for station in stations:
        if station.name in risk_stations_name:
            print("TRUE")
            risk_stations.append(station)


    station1 = risk_stations[0]
    station2 = risk_stations[1]
    station3 = risk_stations[2]
    station4 = risk_stations[3]
    station5 = risk_stations[4]

    dt = 10
    dates1, levels1 = fetch_measure_levels(station1.measure_id, dt=datetime.timedelta(days=dt))
    dates2, levels2 = fetch_measure_levels(station2.measure_id, dt=datetime.timedelta(days=dt))
    dates3, levels3 = fetch_measure_levels(station3.measure_id, dt=datetime.timedelta(days=dt))
    dates4, levels4 = fetch_measure_levels(station4.measure_id, dt=datetime.timedelta(days=dt))
    dates5, levels5 = fetch_measure_levels(station5.measure_id, dt=datetime.timedelta(days=dt))

    plot1 = plot_water_levels(station1, dates1, levels1)
    plot2 = plot_water_levels(station2, dates2, levels2)
    plot3 = plot_water_levels(station3, dates3, levels3)
    plot4 = plot_water_levels(station4, dates4, levels4)
    plot5 = plot_water_levels(station5, dates5, levels5)

    ax1 = plot1.add_subplot(211)

    ax1.plot()

    ax2 = plot2.add_subplot(211)

    ax2.plot()

    ax3 = plot3.add_subplot(211)

    ax3.plot()

    ax4 = plot4.add_subplot(211)

    ax4.plot()

    ax5 = plot5.add_subplot(211)

    ax5.plot()

    plt.show()

if __name__ == "__main__":

    print("*** Task 2E: CUED Part IA Flood Warning System ***")


    run()