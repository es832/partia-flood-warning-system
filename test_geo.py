import pytest
from floodsystem.geo import stations_by_distance
from floodsystem.geo import stations_within_radius
from floodsystem.geo import rivers_by_station_number
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation

stations = build_station_list()



def test_stations_by_distance():

     # Create test station 1
    s_id1 = "test-s-id1"
    m_id1 = "test-m-id1"
    label1 = "some station1"
    coord1 = (0.0, 0.0)
    trange1 = None
    river1 = "River X1"
    town1 = "My Town1"
    s1 = MonitoringStation(s_id1, m_id1, label1, coord1, trange1, river1, town1)

    # Create test station 2
    s_id2 = "test-s-id2"
    m_id2 = "test-m-id2"
    label2 = "some station2"
    coord2 = (1.0, 1.0)
    trange2 = (5.5, 3.4445)
    river2 = "River X2"
    town2 = "My Town2"
    s2 = MonitoringStation(s_id2, m_id2, label2, coord2, trange2, river2, town2)

    # Create test station 3
    s_id3 = "test-s-id3"
    m_id3 = "test-m-id3"
    label3 = "some station3"
    coord3 = (5.0, 5.0)
    trange3 = (-2.3, 3.4445)
    river3 = "River X3"
    town3 = "My Town3"
    s3 = MonitoringStation(s_id3, m_id3, label3, coord3, trange3, river3, town3)

    
    test_list = [s1, s2, s3]

    test_sbd = stations_by_distance(test_list, (0.0, 0.0))

    for i in [0,1]:

        if test_sbd[i][2] <= test_sbd[i+1][2]:
            assert True

        else:
            assert False



        

        

    




def test_stations_within_radius():

    stations = build_station_list()

    stations = build_station_list()

    testlist = stations_within_radius(stations, (52.2053, 0.1218), 10)

    for i in testlist: 

        assert type(i) == str #the outcome will be true as long as the variable is a string



def test_rivers_with_station():
    
    

     # Create test station 1
    s_id1 = "test-s-id1"
    m_id1 = "test-m-id1"
    label1 = "some station1"
    coord1 = (-2.0, 4.0)
    trange1 = None
    river1 = "River X1"
    town1 = "My Town1"
    s1 = MonitoringStation(s_id1, m_id1, label1, coord1, trange1, river1, town1)

    # Create test station 2
    s_id2 = "test-s-id2"
    m_id2 = "test-m-id2"
    label2 = "some station2"
    coord2 = (-2.0, 4.0)
    trange2 = (5.5, 3.4445)
    river2 = "River X2"
    town2 = "My Town2"
    s2 = MonitoringStation(s_id2, m_id2, label2, coord2, trange2, river2, town2)

    # Create test station 3
    s_id3 = "test-s-id3"
    m_id3 = "test-m-id3"
    label3 = "some station3"
    coord3 = (-2.0, 4.0)
    trange3 = (-2.3, 3.4445)
    river3 = "River X3"
    town3 = "My Town3"
    s3 = MonitoringStation(s_id3, m_id3, label3, coord3, trange3, river3, town3)

    
    test_list = [s1, s2, s3]
    set_of_rivers = rivers_with_station(test_list)
    known_river_set = {river1, river2, river3}

    assert set_of_rivers == known_river_set


def test_stations_by_river():
    
    
    

     # Create test station 1
    s_id1 = "test-s-id1"
    m_id1 = "test-m-id1"
    label1 = "some station1"
    coord1 = (-2.0, 4.0)
    trange1 = None
    river1 = "River X1"
    town1 = "My Town1"
    s1 = MonitoringStation(s_id1, m_id1, label1, coord1, trange1, river1, town1)

    # Create test station 2
    s_id2 = "test-s-id2"
    m_id2 = "test-m-id2"
    label2 = "some station2"  
    coord2 = (-2.0, 4.0)
    trange2 = (5.5, 3.4445)
    river2 = "River X1" # Note that river1 and river 2 are the same, so they sholdn't create 2 keys
    town2 = "My Town2"
    s2 = MonitoringStation(s_id2, m_id2, label2, coord2, trange2, river2, town2)

    # Create test station 3
    s_id3 = "test-s-id3"
    m_id3 = "test-m-id3"
    label3 = "some station3"
    coord3 = (-2.0, 4.0)
    trange3 = (-2.3, 3.4445)
    river3 = "River X3"
    town3 = "My Town3"
    s3 = MonitoringStation(s_id3, m_id3, label3, coord3, trange3, river3, town3)

    
    test_list = [s1, s2, s3]
    test_river_dict = stations_by_river(test_list)
    known_river_dict = {river1 : [label1, label2], river3 : [label3]}

    assert test_river_dict == known_river_dict
    


















def test_rivers_by_station_number():
    stations = build_station_list()
    N_rivers = rivers_by_station_number(stations, 5)

    for i in range(len(N_rivers)-1):
        if N_rivers[i][1] >= N_rivers[i+1][1]:
            assert True

        else:
            assert False



