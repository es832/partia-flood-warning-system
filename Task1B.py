from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1B"""
# I am creating a list named actual of ordered stations using the function stations by distance
stations_list = build_station_list()
actual = stations_by_distance( stations_list, (52.2053, 0.1218) )
for i in range(0,10):
    print (actual[i])
for i in range (-11, 0):
    print (actual[i])

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()