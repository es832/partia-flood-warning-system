# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""


from haversine import haversine, Unit




def stations_by_distance(stations, p):

   #Following code calls the coordinate of the station and make an 
    distance=[]
    for x in range(len(stations)):
        distance.append (haversine( stations[x].coord, p))
    # sbd stands for station by distance and is the list that will store the station and distance tuples
    sbd = []
    for i in range(len(stations)):
        sbd.append(tuple([stations[i].name, stations[i].town, distance[i]]))
    sbd.sort(key=lambda x: x[2])
    
    return sbd

    
    
    
    

def stations_within_radius(stations, centre, r):

    length = stations_by_distance (stations, centre)
    namelist = []
    
    for i in range(len(stations)):
        if length[i][2] < r:
            namelist.append(length[i][0])
    

    """ 
    This function returns a list of all stations within the radius, r from the centre in any order assuming that the Lat and Long coordinates are correct.
    """

    namelist.sort()
    


    return namelist




def rivers_with_station(stations):
    
    
    
    # Create an empty set where to add river names. The set ensures that the river name is added only if it is NOT already present in the set.
    set_of_rivers = set()

# for each list in stations, check if it has a river associated with it, and if it does, add it to the set of river names. 
# Add automatically checks and only adds unique names of rivers
    for i in range(len(stations)):
        if stations[i].river != None:
            set_of_rivers.add(stations[i].river)

    return set_of_rivers


def stations_by_river(stations):
    river_dict = dict()

    for i in range(len(stations)):
        if stations[i].river in river_dict:
            river_dict[stations[i].river].append(stations[i].name)

        elif stations[i].river != None:
            river_dict[stations[i].river]=[stations[i].name]
    #river_dict_sorted=sorted(river_dict.keys(), key=lambda x:x.lower())

    return river_dict


def rivers_by_station_number(stations, N):
    # I am creating a dictionsary with river names and its corresponding monitoricng station
    river_dict = stations_by_river(stations)

    # Creating an empty list where I am going to append tuples containg rivers and their station number
    stations_number_list = []

    for key in river_dict:
        stations_number_list.append(tuple((key,len(river_dict[key]))) )

    stations_number_list.sort(key=lambda tup: tup[1], reverse=True)

    N_rivers = stations_number_list[:N]
    while stations_number_list[len(N_rivers)][1] == N_rivers[-1][1]:
        N_rivers.append(stations_number_list[len(N_rivers)])

    return N_rivers





