
from .station import MonitoringStation




def stations_level_over_threshold(stations, tol):
    stations_over_list= []

    for i in stations:
        if MonitoringStation.relative_water_level(i) != None:
            

            if MonitoringStation.relative_water_level(i) > tol:
                stations_over_list.append(tuple((i.river, MonitoringStation.relative_water_level(i))))
    stations_over_list.sort(key=lambda tup: tup[1], reverse=True)

    return stations_over_list 


def stations_highest_rel_level(stations, N):
    stations_over_list= []

    for i in stations:
        if MonitoringStation.relative_water_level(i) != None:
            stations_over_list.append(tuple((i.name, MonitoringStation.relative_water_level(i))))

    stations_over_list.sort(key=lambda tup: tup[1], reverse=True)

    stations_over_list = stations_over_list[0:N]

    return stations_over_list 