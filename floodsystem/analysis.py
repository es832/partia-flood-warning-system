import numpy as np
import matplotlib
from datetime import date, time, datetime



def polyfit(dates, levels, p):
    
    #if length of dates or levels == 0, do nothing

    if len(dates) == 0 or len(levels) == 0:

            return (None,None)

    else:

        float_date = matplotlib.dates.date2num(dates)

        dT=float_date[0]

        p_coeff = np.polyfit(float_date-dT, levels, p)

    

        #convert into usable polynomial function

        poly = np.poly1d(p_coeff)

    

    #to accommodate for poly not being referenced before attempted assignment

    #this would happen if the if statement above were fulfilled

    try:

        return poly, dT

    except:

        pass