import matplotlib.pyplot as plt
import matplotlib
from floodsystem.analysis import polyfit
from datetime import datetime, timedelta
from floodsystem.station import MonitoringStation

# a function that displays a plot of the water level data against time for a station
def plot_water_levels(station, dates, levels):   
    low = station.typical_range[0]
    high = station.typical_range[1]
    list_low = [low]*len(dates)
    list_high = [high]*len(dates)

# Plot
    plt.plot(dates, levels, label="Water Levels")

    plt.plot(dates, list_low, label="Typical Low")

    plt.plot(dates, list_high, label="Typical High")

# Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

# Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels

    return plt.figure()


#
def plot_water_level_with_fit(station, dates, levels, p):
    if len(dates) == 0 or len(levels) == 0:

        print("not enough data available for {}".format(station.name))

    

    else:    

        dates_float = matplotlib.dates.date2num(dates)

        poly = polyfit(dates, levels, p) [0]

        dT = polyfit(dates, levels, p) [1]

        

        low = station.typical_range[0]

        high = station.typical_range[1]

        low_list = [low]*len(dates)

        high_list = [high]*len(dates)

        

        #plot original data points

        plt.plot(dates_float, levels, label="Original")

    

        #plot best-fit polynomial

        plt.plot(dates_float, poly(dates_float-dT), label="Polynomial")

        

        plt.plot(dates_float, low_list, label="Low")

        plt.plot(dates_float, high_list, label="High")

    

        plt.xlabel('date')

        plt.ylabel('water level (m)')

        plt.legend()

        plt.xticks(rotation=45)

        plt.title(station.name)

        

        #Display plot

        plt.tight_layout

        

        return plt.figure()