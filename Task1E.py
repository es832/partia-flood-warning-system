

from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number

def run():
    """Requirements for Task 1E"""

stations = build_station_list()

rivers = stations_by_river(stations)
print(len(rivers))

print((rivers["River Aire"]))
print((rivers["River Cam"]))
print((rivers["River Thames"]))

print(rivers_by_station_number(stations, 10))
if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    run()
