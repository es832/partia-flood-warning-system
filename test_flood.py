import pytest
from floodsystem.flood import  stations_level_over_threshold, stations_highest_rel_level
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation

def test_relative_water_level():

    # Create test station 1
    s_id1 = "test-s-id1"
    m_id1 = "test-m-id1"
    label1 = "some station1"
    coord1 = (-2.0, 4.0)
    trange1 = None
    river1 = "River X1"
    town1 = "My Town1"
    s1 = MonitoringStation(s_id1, m_id1, label1, coord1, trange1, river1, town1)


    # Create test station 2
    s_id2 = "test-s-id2"
    m_id2 = "test-m-id2"
    label2 = "some station2"
    coord2 = (-2.0, 4.0)
    trange2 = (0, 1)
    river2 = "River X2"
    town2 = "My Town2"
    s2 = MonitoringStation(s_id2, m_id2, label2, coord2, trange2, river2, town2)


    # Create test station 3
    s_id3 = "test-s-id3"
    m_id3 = "test-m-id3"
    label3 = "some station3"
    coord3 = (-2.0, 4.0)
    trange3 = (1, 0)
    river3 = "River X3"
    town3 = "My Town3"
    s3 = MonitoringStation(s_id3, m_id3, label3, coord3, trange3, river3, town3)
   

    
    test_list = [s1, s2, s3]
    test_list[0].latest_level= 0
    test_list[1].latest_level= 0.5
    test_list[2].latest_level= 1
    

    assert MonitoringStation.relative_water_level(test_list[0]) == None
    assert MonitoringStation.relative_water_level(test_list[1]) == 0.5
    assert MonitoringStation.relative_water_level(test_list[2]) == None


def test_stations_level_over_threshold():

    # Create test station 1
    s_id1 = "test-s-id1"
    m_id1 = "test-m-id1"
    label1 = "some station1"
    coord1 = (-2.0, 4.0)
    trange1 = None
    river1 = "River X1"
    town1 = "My Town1"
    s1 = MonitoringStation(s_id1, m_id1, label1, coord1, trange1, river1, town1)


    # Create test station 2
    s_id2 = "test-s-id2"
    m_id2 = "test-m-id2"
    label2 = "some station2"
    coord2 = (-2.0, 4.0)
    trange2 = (0, 1)
    river2 = "River X2"
    town2 = "My Town2"
    s2 = MonitoringStation(s_id2, m_id2, label2, coord2, trange2, river2, town2)


    # Create test station 3
    s_id3 = "test-s-id3"
    m_id3 = "test-m-id3"
    label3 = "some station3"
    coord3 = (-2.0, 4.0)
    trange3 = (1, 0)
    river3 = "River X3"
    town3 = "My Town3"
    s3 = MonitoringStation(s_id3, m_id3, label3, coord3, trange3, river3, town3)
   

    
    test_list = [s1, s2, s3]
    test_list[0].latest_level= 0
    test_list[1].latest_level= 0.9
    test_list[2].latest_level= 1
    

    assert stations_level_over_threshold(test_list, 0.8) == [('River X2', 0.9)]

def test_stations_highest_rel_level():

     # Create test station 1
    s_id1 = "test-s-id1"
    m_id1 = "test-m-id1"
    label1 = "some station1"
    coord1 = (-2.0, 4.0)
    trange1 = None
    river1 = "River X1"
    town1 = "My Town1"
    s1 = MonitoringStation(s_id1, m_id1, label1, coord1, trange1, river1, town1)


    # Create test station 2
    s_id2 = "test-s-id2"
    m_id2 = "test-m-id2"
    label2 = "some station2"
    coord2 = (-2.0, 4.0)
    trange2 = (0, 1)
    river2 = "River X2"
    town2 = "My Town2"
    s2 = MonitoringStation(s_id2, m_id2, label2, coord2, trange2, river2, town2)


    # Create test station 3
    s_id3 = "test-s-id3"
    m_id3 = "test-m-id3"
    label3 = "some station3"
    coord3 = (-2.0, 4.0)
    trange3 = (1, 0)
    river3 = "River X3"
    town3 = "My Town3"
    s3 = MonitoringStation(s_id3, m_id3, label3, coord3, trange3, river3, town3)
   

    
    test_list = [s1, s2, s3]
    test_list[0].latest_level= 0
    test_list[1].latest_level= 0.9
    test_list[2].latest_level= 1

    assert stations_highest_rel_level(test_list, 2) != [('River X2', 0.9)]


