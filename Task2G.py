import datetime

from floodsystem.stationdata import build_station_list

from floodsystem.analysis import polyfit

from floodsystem.datafetcher import fetch_measure_levels

from floodsystem.plot import plot_water_level_with_fit

from floodsystem.stationdata import update_water_levels

from floodsystem.flood import stations_highest_rel_level

import matplotlib.pyplot as plt

from floodsystem.stationdata import build_station_list

from floodsystem.stationdata import update_water_levels

from floodsystem.datafetcher import fetch_measure_levels

from floodsystem.flood import stations_level_over_threshold

from floodsystem.plot import plot_water_levels

from floodsystem.flood import stations_highest_rel_level
from floodsystem.station import MonitoringStation

def stations_level_over_threshold_2(stations, tol_higher, tol_lower):
    stations_over_list= []

    for i in stations:
        if MonitoringStation.relative_water_level(i) != None:
            

            if MonitoringStation.relative_water_level(i) > tol_lower and MonitoringStation.relative_water_level(i) <= tol_higher :
                stations_over_list.append(tuple((i, MonitoringStation.relative_water_level(i))))
    stations_over_list.sort(key=lambda tup: tup[1], reverse=True)

    return stations_over_list 



def run():
    """Requirements for Task 2G"""

    stations = build_station_list()
    update_water_levels(stations)

    full_stations_and_score = []

    #for i in stations:
    #    full_stations_and_score.append([i, 0])

    for n in stations_level_over_threshold_2(stations,100 , 0.75):
        full_stations_and_score.append([n[0], 3])

    for n in stations_level_over_threshold_2(stations, 0.75, 0.5):
        full_stations_and_score.append([n[0], 2])

    for n in stations_level_over_threshold_2(stations, 0.5, 0.25):
        full_stations_and_score.append([n[0], 1])

    for n in stations_level_over_threshold_2(stations, 0.25, -100):
        full_stations_and_score.append([n[0], 0])
    
    #print (full_stations_and_score)







    dt=2
    for x in full_stations_and_score[151:346]:

        if x[0].measure_id == None:
            pass

        if x[0].latest_level == None:
            pass

        else:


            dates, levels = fetch_measure_levels(x[0].measure_id, dt=datetime.timedelta(days=dt))

            try:
                poly, _ = polyfit(dates,levels,4)

                future_water_level= poly(-2)
                ratio2 = (future_water_level - x[0].typical_range[0]) / (x[0].typical_range[1]-x[0].typical_range[0])

                if ratio2 > 0.75:
                    x[1]+= 3

                elif ratio2 >0.5 and ratio2 <= 0.75: 
                    x[1] += 2

                elif ratio2 >0.25 and ratio2 <= 0.5: 
                    x[1] += 1

                elif ratio2 >0.25 and ratio2 <= -100: 
                    x[1] += 0
            except:
                pass
            


    severe = []
    high = []
    moderate = []
    low = []

    for m in full_stations_and_score:
        if m[1] == 6:
            severe.append(m[0].name)

        elif m[1] ==5 or m[1] ==4 :
            high.append(m[0].name)

        elif m[1] ==3 or m[1] == 2 :
            moderate.append(m[0].name)

        elif m[1] == 1 or m[1] ==0 :
            low.append(m[0].name) 


    
    print ( "Severe /n", severe)
    print ("High /n", high)

    print ("Moderate /n", moderate)

    print ("Low /n", low)

    
        

        






                





    


    #stations_level_over_threshold(stations, 0.8)









if __name__ == "__main__":

    print("*** Task 2G: CUED Part IA Flood Warning System ***")


    run()