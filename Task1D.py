from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number

def run():
    """Requirements for Task 1D"""

stations = build_station_list()

# Apply the function that creates a list of rivers from the given stations

rivers_with_ms= rivers_with_station(stations)

print(len(rivers_with_ms))


#conver set into list, cause you can't sort or index sets

list_of_rivers = list(rivers_with_ms)
list_of_rivers.sort()

#print items of list within a range

for i in range(0,20):
    
    print (list_of_rivers[i])



rivers = stations_by_river(stations)
print(len(rivers))

print((rivers["River Aire"]))
print((rivers["River Cam"]))
print((rivers["River Thames"]))

print(rivers_by_station_number(stations, 9))

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()
